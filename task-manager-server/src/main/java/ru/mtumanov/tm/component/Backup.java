package ru.mtumanov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    
    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void save() {
        try {
            bootstrap.getDomainService().saveDataBase64();
        } catch (@NotNull final Exception e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void load() {
        try {
            bootstrap.getDomainService().loadDataBase64();
        } catch (@NotNull final Exception e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void stop() {
        executorService.shutdown();
    }

}
