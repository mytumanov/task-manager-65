package ru.mtumanov.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@Repository
public interface IProjectRepository extends JpaRepository<ProjectDTO, String> {

}
