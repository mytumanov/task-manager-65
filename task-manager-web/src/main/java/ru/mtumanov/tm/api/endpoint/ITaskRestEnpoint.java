package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.Collection;

public interface ITaskRestEnpoint {

    @Nullable
    Collection<TaskDTO> findAll();

    @Nullable
    TaskDTO findById(@Nullable final String id);

    @NotNull
    TaskDTO create(@Nullable final TaskDTO task);

    @NotNull
    TaskDTO update(@Nullable final TaskDTO task);

    void delete(@Nullable final TaskDTO task);

    void delete(@Nullable final String id);

}
