package ru.mtumanov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import java.util.Collection;

public interface IProjectRestEndpoint {

    @NotNull
    Collection<ProjectDTO> findAll();

    @Nullable
    ProjectDTO findById(@Nullable final String id);

    @NotNull
    ProjectDTO create(@Nullable final ProjectDTO project);

    @NotNull
    ProjectDTO update(@Nullable final ProjectDTO project);

    void delete(@Nullable final ProjectDTO project);

    void delete(@Nullable final String id);

}
