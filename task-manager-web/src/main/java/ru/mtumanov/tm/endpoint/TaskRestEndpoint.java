package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.mtumanov.tm.api.endpoint.ITaskRestEnpoint;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEnpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @NotNull
    @GetMapping
    public Collection<TaskDTO> findAll() {
        List<TaskDTO> projects = taskService.findAll();
        if (projects == null)
            return Collections.emptyList();
        return projects;
    }

    @Override
    @Nullable
    @GetMapping("/{id}")
    public TaskDTO findById(@PathVariable("id") @Nullable final String id) {
        return taskService.findById(id);
    }

    @Override
    @NotNull
    @PostMapping
    public TaskDTO create(@RequestBody @Nullable final TaskDTO project) {
        return taskService.add(project);
    }

    @Override
    @DeleteMapping
    public void delete(@RequestBody @Nullable final TaskDTO project) {
        taskService.remove(project);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @Nullable final String id) {
        taskService.removeById(id);
    }

    @Override
    @NotNull
    @PutMapping
    public TaskDTO update(@RequestBody @Nullable final TaskDTO project) {
        return taskService.update(project);
    }
}
