package ru.mtumanov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserChangePasswordRq;
import ru.mtumanov.tm.dto.response.user.UserChangePasswordRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class UserChangePasswordListener extends AbstractUserListener {

    @Override
    @NotNull
    public String getDescription() {
        return "change current user password";
    }

    @Override
    @NotNull
    public String getName() {
        return "change-user-password";
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRq request = new UserChangePasswordRq(getToken(), password);
        @NotNull final UserChangePasswordRs response = getUserEndpoint().userChangePassword(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
