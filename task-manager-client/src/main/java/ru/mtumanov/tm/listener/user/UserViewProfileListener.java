package ru.mtumanov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.user.UserProfileRq;
import ru.mtumanov.tm.dto.response.user.UserProfileRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class UserViewProfileListener extends AbstractUserListener {

    @Override
    @NotNull
    public String getDescription() {
        return "view current user profile";
    }

    @Override
    @NotNull
    public String getName() {
        return "view-user-profile";
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER VIEW PROFILE]");
        @NotNull final UserProfileRq request = new UserProfileRq(getToken());
        @NotNull final UserProfileRs response = getUserEndpoint().userProfile(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        showUser(response.getUser());
    }

}
